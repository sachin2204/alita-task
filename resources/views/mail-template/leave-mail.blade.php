<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div class="container">
        Hello {{ $Employees->name }} <br><br>

        Your leave has been {{ $EmployeeLeave->staus }}. <br>
        @if ($EmployeeLeave->staus=='Reject')
            Reason : {{ $reason }}<br>
        @endif
        Date : {{Carbon\Carbon::parse($EmployeeLeave->date)->format('Y-M-d') }} <br>
    </div>
</body>
</html>
