<x-companies-guest-layout>
    @section('content')
    <div class="container ">
        <div class="row justify-content-center mt-5">
            <div class="col-md-5 border-1 p-5 BorderStyle">
                <div>
                    <h3>Companies-Login</h3>
                </div>
                <form action="{{ route('companies.login') }}" method="POST" id="CompanyLogin">
                    @csrf
                    <div class="mb-3">
                      <label  class="form-label">Email address</label>
                      <input type="email" name="email" class="form-control">
                      <x-input-error :messages="$errors->get('email')" class="mt-2" />
                    </div>
                    <div class="mb-3">
                      <label  class="form-label">Password</label>
                      <input type="password" name="password" class="form-control" >
                      <x-input-error :messages="$errors->get('password')" class="mt-2" />
                    </div>
                   <div class="d-flex">
                    <div class="me-2">
                        <a href="{{ route('employees.login') }}" >Employee</button>
                    </div>
                    <div class="me-2">
                        <a href="{{ route('companies.register') }}" >Register</button>
                    </div>
                    <div>
                         <button type="submit" class="btn btn-sm btn-primary">Submit</button>
                    </div>
                   </div>
                </form>
            </div>
        </div>
       
    </div>
    @endsection
</x-companies-guest-layout>
