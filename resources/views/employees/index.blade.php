<x-companies-app-layout>
    @section('content')
    <div class="container mt-5">
        <div class="row">
            <div class="col align-self-start">
                <form action="{{ route('companies.employees.index') }}" method="get">
                 <input type="text" name="search" value="{{ Request::get('search') }}" class="form-control" placeholder="Search">
                </form>
            </div>
            <div class="col align-self-center">
            
            </div>
            <div class="col align-self-end">
                <a href="{{ route('companies.employees.create') }}" class="btn btn-sm btn-info">Add Employee</a>
            </div>
        </div>
        <div class="">
            <table id="example" class="table table-striped" style="width:100%">
                <thead>
                    <tr>
                        <th>Emp Id</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $value)
                    <tr>
                        <td>Emp{{ $value->id }}</td>
                        <td>{{ $value->name }}</td>
                        <td>{{ $value->email }}</td>
                        <td>
                            <div class="d-flex">
                                <div>
                                    <a href="{{ route('companies.employees.edit',$value->id) }}" class="btn btn-sm btn-primary">Edit</a>
                                </div>
                                <div class="ms-2">
                                    <form action="{{ route('companies.employees.destroy',$value->id) }}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button type="button" onclick="DeleteFunction(this)" class="btn btn-sm btn-danger">Delete</a>
                                    </form>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $data->links() }}
        </div>
    </div>
    @endsection
</x-companies-app-layout>