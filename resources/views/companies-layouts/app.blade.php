<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>
        <script type="text/javascript">
          var base_url ="{{ url('/') }}";
        </script>
        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,500,600&display=swap" rel="stylesheet" />

        @include('components.header-links')
      
      
        <!-- Scripts -->
    </head>
    <body >
        <div class="container mt-2">
            <div class="row">
              <div class="col align-self-start">
                <a href="{{ route('companies.dashboard') }}" class="card-link">Dashboard</a>
              </div>
              <div class="col align-self-center">
               
              </div>
              <div class="col align-self-end">
                <form action="{{ route('companies.logout') }}" method="POST">
                    @csrf
                    <button type="submit" class="btn btn-sm btn-danger">Logout</a>
                </form>
              </div>
            </div>
        </div>
          
  
        @yield('content')
    </body>

    <script>
      @if (Session::has('success'))
          Swal.fire("{{ Session::get('success') }}");
      @endif
  
      @if (Session::has('error'))
          Swal.fire("{{ Session::get('error') }}");
      @endif
  </script>
</html>
