<x-companies-app-layout>
    @section('content')
    <div class="container mt-5">
        <div class="d-flex mt-5">
            <div class="card me-5" style="width: 18rem;">
                <div class="card-body">
                  <h5 class="card-title">Employees</h5>
                  <a href="{{ route('companies.employees.index') }}" class="card-link">Employees Listing</a>
                </div>
              </div>
              <div class="card" style="width: 18rem;">
                <div class="card-body">
                  <h5 class="card-title">Employees leaves</h5>
                  <a href="{{ route('companies.Companyindex') }}" class="card-link">Leaves</a>
                </div>
              </div>
        </div>
        
    </div>
    @endsection
</x-companies-app-layout>