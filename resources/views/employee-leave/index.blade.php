<x-companies-app-layout>
    @section('content')
    <div class="container mt-5">
        <div class="row">
            <div class="col align-self-start">
                <form action="{{ route('companies.Companyindex') }}" method="get">
                 <input type="text" name="search" value="{{ Request::get('search') }}" class="form-control" placeholder="Search">
                </form>
            </div>
            <div class="col align-self-center">
            
            </div>
        </div>
        <div class="">
            <table id="example" class="table table-striped" style="width:100%">
                <thead>
                    <tr>
                        <th>Emp Id</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Leave Type</th>
                        <th>Reason</th>
                        <th>Date</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $value)

                    <tr>
                        <td>Emp{{ $value->empId }}</td>
                        <td>{{ $value->name }}</td>
                        <td>{{ $value->email }}</td>
                        <td>{{ $value->leave_type }}</td>
                        <td>{{ $value->reason }}</td>
                        <td>{{ $value->date }}</td>
                        <td>{{ $value->staus }}</td>
                        <td>
                            <div class="d-flex">
                                <div>
                                    @if ($value->staus=='Pending')
                                        <button class="btn btn-sm btn-primary" onclick="ChangeStatus({{ $value->id }})">{{ $value->staus }}</button>
                                    @endif
                                    {{-- <a href="{{ route('companies.employeesStatus',['status'=>$value->staus,'value'=>$value->staus]) }}" class="btn btn-sm btn-primary">Change</a> --}}
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $data->links() }}
        </div>
    </div>
    @endsection
</x-companies-app-layout>