<x-companies-app-layout>
    @section('content')
    <div class="container ">
        <div class="row justify-content-center mt-5">
            <div class="col-md-5 border-1 p-5 BorderStyle">
                <div>
                    <h3>Employee-Edit</h3>
                </div>
                <form action="{{ route('companies.employees.update',$data->id) }}" method="POST" id="EmployeeEdit">
                  @method('PUT')
                    @csrf
                    <div class="mb-3">
                      <label  class="form-label">Employee Name</label>
                      <input type="text" value="{{ $data->name }}" name="name" class="form-control">
                      <x-input-error :messages="$errors->get('name')" class="mt-2" />
                    </div>
                    <div class="mb-3">
                        <label  class="form-label">Employee Email address</label>
                        <input type="email" value="{{ $data->email }}" name="email" class="form-control">
                        <x-input-error :messages="$errors->get('email')" class="mt-2" />
                      </div>
                    <div class="mb-3">
                      <label class="form-label">Employee Password</label>
                      <input type="password" name="password" id="password" class="form-control" >
                      <x-input-error :messages="$errors->get('password')" class="mt-2" />
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Confirm Password</label>
                        <input type="password" name="password_confirmation" class="form-control" >
                        <x-input-error :messages="$errors->get('password_confirmation')" class="mt-2" />
                      </div>
                   <div class="d-flex">
                    <div>
                         <button type="submit" class="btn btn-sm btn-primary">Submit</button>
                        <a href="{{ route('companies.employees.index') }}" class="btn btn-sm btn-danger">Cancel</a>
                    </div>
                   </div>
                </form>
            </div>
        </div>
       
    </div>
    @endsection
</x-companies-app-layout>