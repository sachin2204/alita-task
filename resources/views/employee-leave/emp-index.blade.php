<x-employees-app-layout>
    @section('content')
    <div class="container mt-5">
        <div class="row">
            <div class="col align-self-start">
                <form action="{{ route('employees.employees-leaves.index') }}" method="get">
                 <input type="text" name="search" value="{{ Request::get('search') }}" class="form-control" placeholder="Search">
                </form>
            </div>
            <div class="col align-self-center">
            
            </div>
            <div class="col align-self-end">
                <a href="{{ route('employees.employees-leaves.create') }}" class="btn btn-sm btn-info">Apply Leaves</a>
            </div>
        </div>
        <div class="">
            <table id="example" class="table table-striped" style="width:100%">
                <thead>
                    <tr>
                        <th>Emp Id</th>
                        <th>Leave Type</th>
                        <th>Reason</th>
                        <th>Date</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $value)

                    <tr>
                        <td>Emp{{ $value->empId }}</td>
                        <td>{{ $value->leave_type }}</td>
                        <td>{{ $value->reason }}</td>
                        <td>{{ $value->date }}</td>
                        <td>{{ $value->staus }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $data->links() }}
        </div>
    </div>
    @endsection
</x-employees-app-layout>