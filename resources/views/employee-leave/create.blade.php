<x-employees-app-layout>
    @section('content')
    <div class="container ">
        <div class="row justify-content-center mt-5">
            <div class="col-md-5 border-1 p-5 BorderStyle">
                <div>
                    <h3>Apply-Leave</h3>
                </div>
                <form action="{{ route('employees.employees-leaves.store') }}" method="POST" id="leaveadd">
                    @csrf
                    <div class="mb-3">
                      <label  class="form-label">Leave Type</label>
                      <select name="leave_type" id="" class="form-control">
                        <option value="" disabled selected>Select Option</option>
                        <option value="CL">Casual Leave</option>
                        <option value="PL">Paid Leave</option>
                        <option value="SL">Sick Leave</option>
                        <option value="LWP">Leave Without Pay</option>
                      </select>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Date</label>
                        <input type="date" name="date" min="{{ Carbon\Carbon::now()->addDays(1)->format('Y-m-d') }}" class="form-control" >
                      </div>
                   <div class="d-flex">
                    <div>
                         <button type="submit" class="btn btn-sm btn-primary">Submit</button>
                        <a href="{{ route('employees.employees-leaves.index') }}" class="btn btn-sm btn-danger">Cancel</a>

                    </div>
                   </div>
                </form>
            </div>
        </div>
       
    </div>
    @endsection
</x-employees-app-layout>