<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employees;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use Auth;
class EmployeesController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $query = Employees::where('companie_id',Auth::guard('companies')->user()->id);

        if (isset($request->search)) {
            $query->where('name','like','%'.$request->search.'%')->orWhere('email','like','%'.$request->search.'%');
        }
        $data = $query->orderBy('id','desc')->paginate(10);
        return view('employees.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('employees.create');
        
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'lowercase', 'email', 'max:255', 'unique:'.Employees::class],
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);
        $data = new Employees();
        $data->companie_id = Auth::guard('companies')->user()->id;
        $data->name = $request->name;
        $data->email = $request->email;
        $data->password = Hash::make($request->password);
        if ($data->save()) {
            return redirect()->route('companies.employees.index')->with('success','Employee Created Successfully');
        } else {
            return redirect()->route('companies.employees.create')->with('error','Something Went Wrong!');
        }
        
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data = Employees::find($id);
        return view('employees.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'lowercase', 'email', 'max:255', 'unique:employees,email,'.$id],
        ]);
        $data = Employees::find($id);
        $data->name = $request->name;
        $data->email = $request->email;
        if (isset($request->password)) {
            $data->password = Hash::make($request->password);
        }
        if ($data->save()) {
            return redirect()->route('companies.employees.index')->with('success','Employee Updated Successfully');
        } else {
            return redirect()->route('companies.employees.edit',$id)->with('error','Something Went Wrong!');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        if (Employees::where('id',$id)->delete()) {
            return redirect()->route('companies.employees.index')->with('success','Employee Deleted Successfully');
        } else {
            return redirect()->route('companies.employees.index')->with('error','Something Went Wrong!');
        }
        
    }
}
