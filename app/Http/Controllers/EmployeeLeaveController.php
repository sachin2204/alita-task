<?php

namespace App\Http\Controllers;

use App\Models\EmployeeLeave;
use Illuminate\Http\Request;
use Auth;
use Mail;
use App\Models\Employees;
use Carbon\Carbon;
class EmployeeLeaveController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $query = EmployeeLeave::leftJoin('employees','employees.id','=','employee_leave.employee_id')
                ->leftJoin('companies','companies.id','=','employees.companie_id')
                ->where('employee_leave.employee_id',Auth::guard('employees')->user()->id)
                ->select('employee_leave.id','employee_leave.leave_type','employee_leave.staus',
                'employee_leave.reason','employee_leave.date');
        if (isset($request->search)) {
            $query->where('employee_leave.leave_type','like','%'.$request->search.'%')
            ->orWhere('employee_leave.staus','like','%'.$request->search.'%');
        }
        $data = $query->orderBy('employee_leave.id','desc')->paginate(10);
        return view('employee-leave.emp-index',compact('data'));
    }

    public function Companyindex(Request $request)
    {
        $query = EmployeeLeave::leftJoin('employees','employees.id','=','employee_leave.employee_id')
                ->leftJoin('companies','companies.id','=','employees.companie_id')
                ->where('employees.companie_id',Auth::guard('companies')->user()->id)
                ->select('employee_leave.id','employees.id as empId','employees.name','employees.email','employee_leave.leave_type','employee_leave.staus',
                'employee_leave.reason','employee_leave.date');
        if (isset($request->search)) {
            $query->where('employees.name','like','%'.$request->search.'%')
            ->orWhere('employees.email','like','%'.$request->search.'%')
            ->orWhere('employee_leave.leave_type','like','%'.$request->search.'%')
            ->orWhere('employee_leave.staus','like','%'.$request->search.'%')
            ->orWhere('employees.id','like','%'.$request->search.'%');
        }
        $data = $query->orderBy('employee_leave.id','desc')->paginate(10);
        return view('employee-leave.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('employee-leave.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {   
        $date = Carbon::parse($request->date);
        $month = $date->month;
        $year = $date->year;
        if ($request->leave_type!='LWP') {
            $CheckMonthly = EmployeeLeave::where('employee_id',Auth::guard('employees')->user()->id)
                ->where('leave_type',$request->leave_type)
                ->whereIn('staus',['Pending','Reject'])
                ->whereMonth('date', '=', $month)
                ->count();
        
            $CheckYear = EmployeeLeave::where('employee_id',Auth::guard('employees')->user()->id)->where('leave_type',$request->leave_type)
                    ->whereIn('staus',['Pending','Reject'])
                    ->whereYear('date', '=', $year)
                    ->count();
            if ($CheckMonthly>=2 || $CheckYear>=6) {
                $msg = 'You Cannot Take '.$request->leave_type;
            return redirect()->route('employees.employees-leaves.create')->with('error',$msg);
            }
        }
       
        $data = new EmployeeLeave();
        $data->employee_id = Auth::guard('employees')->user()->id;
        $data->leave_type = $request->leave_type;
        $data->date = $request->date;
        $data->staus = 'Pending';
        if ($data->save()) {
            return redirect()->route('employees.employees-leaves.index')->with('success','Employee-Leave Created Successfully');
        } else {
            return redirect()->route('employees.employees-leaves.create')->with('error','Something Went Wrong!');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(EmployeeLeave $employeeLeave)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(EmployeeLeave $employeeLeave)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, EmployeeLeave $employeeLeave)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(EmployeeLeave $employeeLeave)
    {
        //
    }

    public function employeesStatus(Request $request) {
        $EmployeeLeave = EmployeeLeave::find($request->val);
        $EmployeeLeave->staus = $request->status;
        $EmployeeLeave->reason = $request->reason;
        $EmployeeLeave->save();
        $reason = $request->reason;
        $status = $request->status;
        $Employees = Employees::find($EmployeeLeave->employee_id);
        $SendEmail = $SendEmail->email;
        Mail::send('mail-template.leave-mail',compact('EmployeeLeave','reason','Employees'),function($message)use($SendEmail,$status){
                $message->to($SendEmail);
                $message->subject('Leave is '.$status);
        });
        return json_encode(['status'=>true,'msg'=>$status]);
    }
}
