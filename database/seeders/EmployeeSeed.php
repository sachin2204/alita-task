<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Employees;
use Hash;
class EmployeeSeed extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        for ($i=1; $i <= 10; $i++) { 
           $data = new Employees();
           $data->companie_id = 1;
           $data->name = 'Emp'.$i;
           $data->email  = 'emp'.$i.'@gmail.com';
           $data->password  = Hash::make('123456789');
           $data->save();
        }
    }
}
