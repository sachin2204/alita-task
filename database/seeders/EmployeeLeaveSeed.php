<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\EmployeeLeave;

class EmployeeLeaveSeed extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        for ($i=1; $i <= 3; $i++) { 
            $data = new EmployeeLeave();
            $data->employee_id = $i;
            $data->leave_type = 'CL';
            $data->staus  = 'Pending';
            $data->date  = now();
            $data->save();
         }
    }
}
