<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Companies;
use Hash;
class CompaniesSeeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $Companies =  Companies::where('email','company1@gmail.com')->first();
        if (isset($Companies)) {
            return ;
        }
        $data = new Companies();
        $data->name = 'company1';
        $data->email  = 'company1@gmail.com';
        $data->password  = Hash::make('123456789');
        $data->save();
    }
}
