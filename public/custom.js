
$(document).ready(function () {


    $('#regForm').validate({
        rules: {
          name: {
               required: true,
           },
           email: {
              required: true,
              email:true
          },
            password: {
              required: true,
              minlength: 8,
          },
          password_confirmation: {
              required: true,
              equalTo:'#password'
          }
        },
        messages: {
          name: {
               required: "This field is required",
           },
           email: {
              required: "This field is required",
              email:"Invalid Email Address"
          },
          password: {
              required: "This field is required",
              minlength:'Password required Minlength 8'
          },
          password_confirmation: {
              required: "This field is required",
              equalTo: "Password Not Match",
          }
        }
      });


      $('#CompanyLogin').validate({
        rules: {
         
           email: {
              required: true,
              email:true
          },
            password: {
              required: true,
          }
        },
        messages: {
           email: {
              required: "This field is required",
              email:"Invalid Email Address"
          },
          password: {
              required: "This field is required",
          }
        }
      });

      $('#employeeLogin').validate({
        rules: {
         
           email: {
              required: true,
              email:true
          },
            password: {
              required: true,
          }
        },
        messages: {
           email: {
              required: "This field is required",
              email:"Invalid Email Address"
          },
          password: {
              required: "This field is required",
          }
        }
      });

      $('#EmployeeAdd').validate({
        rules: {
          name: {
               required: true,
           },
           email: {
              required: true,
              email:true
          },
            password: {
              required: true,
              minlength: 8,
          },
          password_confirmation: {
              required: true,
              equalTo:'#password'
          }
        },
        messages: {
          name: {
               required: "This field is required",
           },
           email: {
              required: "This field is required",
              email:"Invalid Email Address"
          },
          password: {
              required: "This field is required",
              minlength:'Password required Minlength 8'
          },
          password_confirmation: {
              required: "This field is required",
              equalTo: "Password Not Match",
          }
        }
      });
      
      $('#EmployeeEdit').validate({
        rules: {
          name: {
               required: true,
           },
           email: {
              required: true,
              email:true
          },
            password: {
              minlength: 8,
          },
          password_confirmation: {
              equalTo:'#password'
          }
        },
        messages: {
          name: {
               required: "This field is required",
           },
           email: {
              required: "This field is required",
              email:"Invalid Email Address"
          },
          password: {
              minlength:'Password required Minlength 8'
          },
          password_confirmation: {
              equalTo: "Password Not Match",
          }
        }
      });

      $('#leaveadd').validate({
        rules: {
          leave_type: {
               required: true,
           },
           date: {
              required: true,
              date: true,
          }
        },
        messages: {
          name: {
               required: "This field is required",
           },
           date: {
              required: "This field is required",
              date: "Date is required",
        },
        }
      });
});

function DeleteFunction(params) {
  var data = $(params);
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!"
    }).then((result) => {
      if (result.isConfirmed) {
        $(data).closest('form').submit();
      }
    });
}

function ChangeStatus(params) {
  console.log(params);

  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: "btn btn-success",
      cancelButton: "btn btn-danger me-2"
    },
    buttonsStyling: false
  });
  swalWithBootstrapButtons.fire({
    title: "Are you sure?",
    text: "You won't be able to revert this!",
    icon: "warning",
    showCancelButton: true,
    confirmButtonText: "Approved",
    cancelButtonText: "Reject",
    reverseButtons: true
  }).then((result) => {
    if (result.isConfirmed) {
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
      $.ajax({
        type: "POST",
        url: base_url+"/companies/employees-status",
        data: {
          val:params,
          status:'Approved'
        },
        dataType: "JSON",
        success: function (response) {
          Swal.fire({
            title: "Status Changed",
            confirmButtonText: "Save",
          }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
             location.reload();
            } 
          });
        }
      });
  
    } else if (
      /* Read more about handling dismissals below */
      result.dismiss === Swal.DismissReason.cancel
    ) {
      Swal.fire({
        title: "Enter the Reason",
        input: "text",
        inputLabel: "Reason",
        showCancelButton: true,
        inputValidator: (value) => {
          if (!value) {
            return "You need to write something!";
          }else{
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
              type: "POST",
              url: base_url+"/companies/employees-status",
              data: {
                val:params,
                status:'Reject',
                reason:value,
              },
              dataType: "JSON",
              success: function (response) {
                Swal.fire({
                  title: "Status Changed",
                  confirmButtonText: "Save",
                }).then((result) => {
                  /* Read more about isConfirmed, isDenied below */
                  if (result.isConfirmed) {
                   location.reload();
                  } 
                });

              }
            });
        
          }
        }
      });
    }
  });

}