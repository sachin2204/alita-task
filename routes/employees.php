<?php

use App\Http\Controllers\Employees\AuthenticatedSessionController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EmployeeLeaveController;

Route::group(['middleware'=>['guest:employees'],'prefix'=>'employees','as'=>'employees.'],function () {

    Route::get('login', [AuthenticatedSessionController::class, 'create'])
                ->name('login');

    Route::post('login', [AuthenticatedSessionController::class, 'store']);
});

Route::group(['middleware'=>['auth:employees'],'prefix'=>'employees','as'=>'employees.'],function () {

    Route::get('/dashboard', function () {
        return view('employees-dashboard');
    })->name('dashboard');
    Route::resource('/employees-leaves', EmployeeLeaveController::class);
    Route::post('logout', [AuthenticatedSessionController::class, 'destroy'])
                ->name('logout');
});
