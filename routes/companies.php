<?php

use App\Http\Controllers\Companies\AuthenticatedSessionController;
use App\Http\Controllers\Companies\RegisteredUserController;
use App\Http\Controllers\EmployeesController;
use App\Http\Controllers\EmployeeLeaveController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware'=>['guest:companies'],'prefix'=>'companies','as'=>'companies.'],function () {
    Route::get('register', [RegisteredUserController::class, 'create'])
                ->name('register');

    Route::post('register', [RegisteredUserController::class, 'store']);

    Route::get('login', [AuthenticatedSessionController::class, 'create'])
                ->name('login');

    Route::post('login', [AuthenticatedSessionController::class, 'store']);

});

Route::group(['middleware'=>['auth:companies'],'prefix'=>'companies','as'=>'companies.'],function () {

    Route::post('logout', [AuthenticatedSessionController::class, 'destroy'])
                ->name('logout');

    Route::get('/dashboard', function () {
        return view('companies-dashboard');
    })->name('dashboard');

    Route::resource('/employees', EmployeesController::class);
    Route::resource('/employees-leaves', EmployeeLeaveController::class);
    Route::get('leaves', [EmployeeLeaveController::class, 'Companyindex'])->name('Companyindex');
    
    Route::POST('/employees-status', [EmployeeLeaveController::class, 'employeesStatus'])->name('employeesStatus'); 
    
});
